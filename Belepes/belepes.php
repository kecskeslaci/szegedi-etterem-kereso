<?php
session_start();
require'../menu.php';
require_once('../Config/connection.php');

?>

<!DOCTYPE html>
<html lang="hu">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="../JS/jquery-3.3.1.min.js" charset="UTF-8"></script>
	  <link rel="stylesheet" href="../CSS/bootstrap.min.css">
    <script src="../JS/bootstrap.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="belepes.css">
    <link rel="stylesheet" href="../CSS/reszponziv.css">

    <title>Szegedi Éttermek</title>

  </head>
  <body>


      <nav>
          <?php echo $menu; ?>
      </nav>

      <div class="form"  align="center">
         <form class="flex-container" action="../php/belep.php" method="post">
              <div class="form-group" id="col-75">
                  <label id="label">Felhasználónév</label>
                  <input type="username" class="form-control" name="username" id="exampleFormControlInput1">
              </div>
              <div class="form-group" id="col-75">
                  <label id="label">Jelszó</label>
                  <input type="password" class="form-control" name="password" id="exampleFormControlInput1">
              </div>
              <div class="form-group" id="col-75">
              <button type="submit" name="belep" value="Belép" class="btn btn-dark" id="submitbtn">Belépés</button>

              </div>
              </form>

               <a href="../Regisztracio/regisztracio.php"> <button type="button" name="regist" value="Regiszráció" class="btn btn-dark" >Regisztráció</button></a>

       </div>
</html>
