-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2019. Ápr 01. 10:36
-- Kiszolgáló verziója: 10.1.30-MariaDB
-- PHP verzió: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `ettermek`
--
CREATE DATABASE IF NOT EXISTS `ettermek` DEFAULT CHARACTER SET utf8 COLLATE utf8_hungarian_ci;
USE `ettermek`;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `Comments` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `etteremId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `comments`
--

INSERT INTO `comments` (`id`, `uid`, `Comments`, `etteremId`) VALUES
(1, 8, 'Jó nagyon', 1),
(2, 10, 'Fini', 2),
(3, 11, 'Legjobb', 3),
(4, 10, 'bdfsahjkd', 3),
(5, 10, 'zsíros', 4),
(6, 10, 'Fhu de jó', 4),
(7, 11, 'jó nagyon', 2),
(8, 10, 'dasdas', 4),
(9, 10, 'Finom nagyon', 5),
(10, 10, 'Nagy választék', 5),
(11, 12, 'asdas', 2);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `etterem`
--

CREATE TABLE `etterem` (
  `Nev` varchar(30) COLLATE utf8_hungarian_ci NOT NULL,
  `Tipus` varchar(20) COLLATE utf8_hungarian_ci NOT NULL,
  `Cim` varchar(30) COLLATE utf8_hungarian_ci NOT NULL,
  `Id` int(11) NOT NULL,
  `kep_eleres` varchar(255) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `etterem`
--

INSERT INTO `etterem` (`Nev`, `Tipus`, `Cim`, `Id`, `kep_eleres`) VALUES
('Pizzatorony', '  Pizzázó', '  Szamos u. 4, 6723', 1, 'kepek/pizzatorony.jpg'),
('Pizzaguru', 'Pizzázó', 'Oskola u. 8, 6720', 2, 'kepek/pizzaguru.jpg'),
('Pizza monkey', 'Pizzázó', 'Pille u. 15, 6722', 3, 'kepek/pizzamonkey.jpg'),
('McDonalds', 'Gyorsétterem', 'Kárász u. 11, 6720', 4, 'kepek/mcdonalds.jpg'),
('KFC', 'Gyorsétterem', 'Londoni körút 3', 5, 'kepek/kfc.jpg'),
('BurgerKing', '     Gyorsétterem', '     Jókai utca 1', 6, '     kepek/burgerking.jpg');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `profilkepek`
--

CREATE TABLE `profilkepek` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `profilkepnev` varchar(255) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `profilkepek`
--

INSERT INTO `profilkepek` (`id`, `uid`, `profilkepnev`) VALUES
(9, 11, 'raspberry-2023405_1920.jpg'),
(11, 10, 'magnolia-2832443_1280.jpg');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `user`
--

CREATE TABLE `user` (
  `uid` int(11) NOT NULL,
  `username` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `user`
--

INSERT INTO `user` (`uid`, `username`, `email`, `password`) VALUES
(8, 'Levente', ' valami@valami.com', '$2y$10$eqOZSecp2OcETKj/gekFKupByyYMEm3l9Qaa42kzS2IcVS3ZNZusO'),
(10, 'admin123', 'admin@admin.hu', '$2y$10$MjD5BRIjUSzAmPCJnhaEr.kII9Ex9VesFO5DiidsHDg73sE5E8Hpi'),
(11, 'Márk19', 'marosi@marosi.com', '$2y$10$7PuilXgK5bPdKU3WpHHqIupZc4HSJi8PLbaEdDyQqAJinMdEpNQbW'),
(12, 'GDTudos', 'gd@gd.com', '$2y$10$ZaClw55nLYDstl/r7X88juiN2NeDlt3GuA4Ep1o8k//zeQxQ.DX3G');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `etterem`
--
ALTER TABLE `etterem`
  ADD PRIMARY KEY (`Id`);

--
-- A tábla indexei `profilkepek`
--
ALTER TABLE `profilkepek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT a táblához `profilkepek`
--
ALTER TABLE `profilkepek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT a táblához `user`
--
ALTER TABLE `user`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
