<?php
session_start();
require'../menu.php';
require_once('../Config/connection.php');
$id = $_SESSION['uid'];
$sql = "SELECT * FROM user WHERE uid=$id";


$res = $conn -> query($sql);

if(!$res){
  die("Hiba a lekérdezés során!");
}
$row = $res -> fetch_assoc();
$username = $row["username"];

$sql_profilkep = "SELECT profilkepnev FROM profilkepek WHERE uid = $id";
$res_profilkepek = $conn -> query($sql_profilkep);

$row_profilkepek = $res_profilkepek -> fetch_assoc();

$profilkep = $row_profilkepek['profilkepnev'];

 ?>
 <!DOCTYPE html>
 <html lang="hu">
   <head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <script src="../JS/jquery-3.3.1.min.js" charset="UTF-8"></script>
 	   <link rel="stylesheet" href="../CSS/bootstrap.min.css">
      <script src="../JS/bootstrap.min.js" type="text/javascript"></script>
     <link rel="stylesheet" href="profilmodositas.css">
      <script src="../JS/validacio.js" charset="UTF-8"></script>
     <link rel="stylesheet" href="../CSS/reszponziv.css">



     <title>Szegedi Éttermek</title>


   </head>
   <body>



     <nav>
         <?php echo $menu; ?>
     </nav>


</form>
<div class="form" align="center">

  <img src="../profilkep/kepek/<?php echo $profilkep ?>" id="profilkep" class='img-fluid card-img-right flex-auto d-none d-lg-block' style="height:250px; width: 200px;">

   <form class="flex-container" method="post" action="update.php">
        <div class="form-group" id="col-75">
            <label id="label" >Új felhasználónév*</label>
            <input type="username" name = "user" class="form-control" id="exampleFormControlInput1" value="<?php echo $username ?>" required><br>
            <span id="userError"></span>
        <br>
        </div>
        <div class="form-group" id="col-75">
            <label id="label">Aktuálisjelszó*</label>
            <input type="password" name = "apwd" class="form-control" id="exampleFormControlInput1" required><br>
              <span id="pwdError"></span>
              <br>
        </div>
        <div class="form-group" id="col-75">
            <label id="label">Új jelszó*</label>
            <input type="password" name = "pwd" class="form-control" id="exampleFormControlInput1" required><br>
            	<span id="pwdError"></span>
              <br>
        </div>
		 <div class="form-group" id="col-75">
            <label id="label">Jelszó megerősítése</label>
            <input type="password" name = "pwdc" class="form-control" id="exampleFormControlInput1" required><br>
            <span id="pwdcError"></span>
            <br>
        </div>

        <div class="form-group" id="col-75">
        <button type="submit" value="Módosítás" name="mod" class="btn btn-dark" id="submitbtn">Módosítás</button>
        </div>
</form>
        <form action="../profilkep/profilkep.php" method="post" class="flex-container" enctype="multipart/form-data">
          <div class="form-group" id="col-75">

            <label class="file-upload btn btn-primary form-control" >
                Kép kiválasztása <input type="file" style="width:0px; height:0px;" name="fajl" id="fajl"/>
            </label>
            <div class="form-group" id="col-75">
              <input type="submit" value="Feltöltés" name="upload" class="btn btn-dark" id="submitbtn">
              <div>
          </div>
          </form>

</div>

</body>
</html>
