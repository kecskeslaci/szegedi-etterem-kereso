<?php

session_start();
require'../menu.php';
require_once('../Config/connection.php');

$id = $_GET['id'];
$sql_etterem = "SELECT * FROM etterem WHERE Id = $id";
      $res = $conn -> query($sql_etterem);

      if(!$res){
        die("Hiba a lekérdezés során!");
      }

$sql = "SELECT user.username, comments.Comments, COALESCE (profilkepek.profilkepnev,'flat-2126885_1280.png') AS profilkepnev
        FROM user INNER JOIN comments ON user.uid = comments.uid LEFT JOIN profilkepek ON user.uid = profilkepek.uid
        WHERE comments.etteremId = $id";

$res_comments = $conn -> query($sql);

$uid = $_SESSION['uid'];


$tabla = "";
while ($row = $res -> fetch_assoc()){
$tabla .= "
        <div class='row d-flex '>
        <div class='col-md-4' style='margin-top:200px'>
          <div class='row'>
           <div class='card flex-md-row mb-3 shadow-sm h-md-250'  style='height: 300px'>
              <div class='card-body d-flex flex-column align-items-start'>";

$tabla .=                 "<h6 class='mb-0'>
                    <a class='text-dark' href='#'>{$row['Nev']}</a>
                 </h6>
                 <div class='mb-1 text-muted small'>{$row['Tipus']}</div>
                 <a class='btn btn-outline-primary btn-sm' role='button' href ='https://www.google.com/maps/search/{$row['Cim']}'>{$row['Cim']}</a>

                 <form action='comment.php?id={$row['Id']}' method='POST'>
                     <button type='submit' class='btn btn-outline-primary btn-sm'  id='submitbtn' name='hozzaszolasok' >Hozzászólások</button>
                 </form>

                </div>

              ​<picture>

                <img height='200x250' class='img-fluid img-thumbnail card-img-right flex-auto d-none d-lg-block'  alt='Thumbnail [200x250]' src={$row['kep_eleres']}  alt='Responsive image'  >

</picture>

           </div>
        </div>
      </div>
      </div>'";

    }

 ?>

 <!DOCTYPE html>

 <html lang="hu">

   <head>
     <script src="../JS/jquery-3.3.1.min.js" charset="UTF-8"></script>
     <link rel="stylesheet" href="etterem.css">
     <link rel="stylesheet" href="../CSS/bootstrap.min.css">

     <script src="../JS/bootstrap.min.js" type="text/javascript"></script>
     <script src="../JS/validacio.js" charset="UTF-8"></script>
     <link rel="stylesheet" href="comment.css">
     <link rel="stylesheet" href="../CSS/reszponziv.css">

      <title>Szegedi Éttermek</title>
   </head>

   <body>
     <?php
     echo $tabla;
      ?>
      <div class="row d-flex">


     <form action="commentFeltolt.php?id=<?php echo $id;  ?>" class="flex-container" method="post">
        <textarea class="form-control"  id="comment" style="width: 450px;"  name="comment" cols='40' rows='5' placeholder='Hozzászólás írása' maxlength="500" ></textarea>
        <button type='submit' class='btn btn-primary' id='submitbtn' name='hozzaszolas' >Hozzászólok</button>
    </form><br>
  </div>

    <div class="row d-flex ">
    <form>

    <?php
      while ($row_comments = $res_comments -> fetch_assoc())
      {

          echo "<div class='card' style='width: 18rem;'>
                        <div class='card-body'>
                        <picture>
                          <img id='profkep' class='img-fluid img-thumbnail card-img-right flex-auto d-none d-lg-block' align='right' src='../profilkep/kepek/{$row_comments['profilkepnev']} '   >
                        </picture>
                          <a><h5 class='card-title'>{$row_comments['username']}</h5></a>
                          <p class='card-text'>";

                          echo $row_comments['Comments'];
                          echo '</p>

                        </div>
                      </div>';


      }
     ?>

   </form>

 </div>


  </body>

 </html>
