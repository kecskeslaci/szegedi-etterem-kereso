<?php
session_start();
require'../menu.php';
require_once('../Config/connection.php');

if (isset($_SESSION['uid'])){

    die();
}
  //header('Location: ../Fooldal/fooldal.php');

?>
<!DOCTYPE html>
<html lang="hu">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <script src="../JS/jquery-3.3.1.min.js" charset="UTF-8"></script>
	<link rel="stylesheet" href="../CSS/bootstrap.min.css">
    <script src="../JS/bootstrap.min.js" type="text/javascript"></script>
  <link rel="stylesheet" href="regisztracio.css">
    <script src="../JS/validacio.js" charset="UTF-8"></script>
  <link rel="stylesheet" href="../CSS/reszponziv.css">



    <title>Szegedi Éttermek</title>


  </head>
  <body>

         <div id="card" class="card-rounded text-white text-center  mx-auto  bg-dark ">
            <div class="card-body">
                 <form class="flex-container" method="post" action="../php/reg.php" >
                      <div class="form-group" >
                          <label id="label" >Felhasználónév*</label>
                          <input type="username" name = "user"
                                 class="form-control" id="exampleFormControlInput1"
                                 placeholder="Kakaógengszter" required><br>
                          <span id="userError"></span>
                      <br>
                      </div>
              		<div class="form-group" >
                          <label id="label">Email cím*</label>
                          <input type="email" name = "email" class="form-control" id="exampleFormControlInput1" placeholder="valami@valami.com" required><br>
                          <span id="emailError"></span>
                          <br>
                      </div>
                      <div class="form-group" >
                          <label id="label">Jelszó*</label>
                          <input type="password" name = "pwd" class="form-control" id="exampleFormControlInput1" required><br>
                          	<span id="pwdError"></span>
                            <br>
                      </div>
              		 <div class="form-group" >
                          <label id="label">Jelszó megerősítése</label>
                          <input type="password" name = "pwdc" class="form-control" id="exampleFormControlInput1" required><br>
                          <span id="pwdcError"></span>
                          <br>
                      </div>

                      <div class="form-group">
                      <button type="submit" value="Regisztráció" name="reg" class="btn btn-primary" id="submitbtn" >Regisztráció</button>
                      </div>
                    </form>
              </div>
      </div>
  </body>
</html>
